const express = require('express');
let http = require('http');
let app = express();
let server = http.createServer(app);
const PORT = process.env.PORT || 5000

app.use(express.static('public'));
app.use(express.static('public/webapp.bundle.js'));

app.get('/', function (req, res) {
  res.setHeader('Content-Type: application/javascript; charset=utf-8')
  res.sendFile('public/index.html')
})

app.get('/webapp.bundle.js', function (req, res) {
  res.setHeader('Content-Type: application/javascript; charset=utf-8')
  res.sendFile('public/webapp.bundle.js')
})

server.listen(PORT, () => console.log(`Listening on ${ PORT }`));
